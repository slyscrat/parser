﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
// using Excel = Microsoft.Office.Interop.Excel;
using OfficeOpenXml;

namespace Parser
{
    public partial class Form1 : Form
    {
        public const string pathSource = "https://bdu.fstec.ru/documents/files/thrlist.xlsx";
        public const string pathFile = "C:\\Users\\ilyab\\Downloads\\thrlist.xlsx";
        public static BindingList<Threat> threatList = new BindingList<Threat>();
        public Form1()
        {
            InitializeComponent();
            this.listBox1.DataSource = threatList;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private static void ParseThreats(string fileName)
        {
            /*try
            {
                DateTime start = DateTime.Now;
                Excel.Application app = new Excel.Application();
                Excel.Workbook workbook = app.Workbooks.Open(fileName);
                Excel.Worksheet worksheet = workbook.Sheets[1];
                Excel.Range range = worksheet.UsedRange;

                int row = range.Rows.Count;
                int col = range.Columns.Count;

                for (int i = 3; i <= row; i++)
                {
                    StringBuilder sb = new StringBuilder();
                    for (int j = 1; j <= col; j++)
                    {
                        if (range.Cells[i,j] != null && range.Cells[i, j].Value2 != null)
                        {
                            sb = sb.Append(range.Cells[i, j].Value.ToString() + "||");
                        }
                    }
                    threatList.Add(sb.ToString());
                }

                GC.Collect();
                GC.WaitForPendingFinalizers();

                Marshal.ReleaseComObject(range);
                Marshal.ReleaseComObject(worksheet);

                workbook.Close();

                Marshal.ReleaseComObject(workbook);

                app.Quit();
                Marshal.ReleaseComObject(app);

                DateTime end = DateTime.Now;
                MessageBox.Show((end-start).TotalSeconds.ToString());
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка!");
            }*/

        }

        private void ThreatsParse(string fileName)
        {
            DateTime start = DateTime.Now;
            using (ExcelPackage xlPackage = new ExcelPackage(new FileInfo("C:\\Users\\ilyab\\Downloads\\thrlist.xlsx")))
            {
                var myWorkSheet = xlPackage.Workbook.Worksheets.First();
                var totalRows = myWorkSheet.Dimension.End.Row;
                var totalColumns = myWorkSheet.Dimension.End.Column;

                for (int i = 3; i <= totalRows; i++)
                {
                    var row = myWorkSheet.Cells[i, 1, i, totalColumns].Select(c => c.Value == null ? string.Empty : c.Value.ToString());
                    string.Join("@", row);
                    threatList.Add(MakeThreat(string.Join("@", row).Split('@')));
                    //threatList.Add(string.Join("@", row));
                }
            }
            DateTime end = DateTime.Now;
            MessageBox.Show((end - start).TotalSeconds.ToString());
        }

        private static Threat MakeThreat(string[] strings)
        {
            return new Threat(Int32.Parse(strings[0]), strings[1], strings[2], strings[3], strings[4], strings[5].Equals("1") ? true: false, strings[6].Equals("1") ? true : false, strings[7].Equals("1") ? true : false);
            //return new Threat(Int32.Parse(strings[0]), strings[1], strings[2], strings[3], strings[4], Convert.ToBoolean(strings[5]), Convert.ToBoolean(strings[6]), Convert.ToBoolean(strings[7]));
        }



        private void listBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }
        private void btn_dwnload_Click(object sender, EventArgs e)
        {
            if (!File.Exists(pathFile))
            {
                new WebClient().DownloadFile(pathSource, pathFile);
            }
            else
            {
                MessageBox.Show("Все ок!");
            }

        }
        private void btn_parse_Click(object sender, EventArgs e)
        {
            ThreatsParse(pathSource);
            MessageBox.Show(threatList.Count.ToString());
        }

        private void btn_parse_bad_Click(object sender, EventArgs e)
        {
            ParseThreats(pathSource);
            MessageBox.Show(threatList.Count.ToString());
        }
    }
}
