﻿namespace Parser
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.btn_dwnload = new System.Windows.Forms.Button();
            this.btn_parse = new System.Windows.Forms.Button();
            this.btn_parse_bad = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 16;
            this.listBox1.Location = new System.Drawing.Point(12, 16);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(561, 420);
            this.listBox1.TabIndex = 0;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged_1);
            // 
            // btn_dwnload
            // 
            this.btn_dwnload.Location = new System.Drawing.Point(606, 16);
            this.btn_dwnload.Name = "btn_dwnload";
            this.btn_dwnload.Size = new System.Drawing.Size(168, 38);
            this.btn_dwnload.TabIndex = 1;
            this.btn_dwnload.Text = "Download";
            this.btn_dwnload.UseVisualStyleBackColor = true;
            this.btn_dwnload.Click += new System.EventHandler(this.btn_dwnload_Click);
            // 
            // btn_parse
            // 
            this.btn_parse.Location = new System.Drawing.Point(606, 91);
            this.btn_parse.Name = "btn_parse";
            this.btn_parse.Size = new System.Drawing.Size(168, 38);
            this.btn_parse.TabIndex = 2;
            this.btn_parse.Text = "Parse";
            this.btn_parse.UseVisualStyleBackColor = true;
            this.btn_parse.Click += new System.EventHandler(this.btn_parse_Click);
            // 
            // btn_parse_bad
            // 
            this.btn_parse_bad.Location = new System.Drawing.Point(606, 165);
            this.btn_parse_bad.Name = "btn_parse_bad";
            this.btn_parse_bad.Size = new System.Drawing.Size(168, 38);
            this.btn_parse_bad.TabIndex = 3;
            this.btn_parse_bad.Text = "Parse Bad";
            this.btn_parse_bad.UseVisualStyleBackColor = true;
            this.btn_parse_bad.Click += new System.EventHandler(this.btn_parse_bad_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_parse_bad);
            this.Controls.Add(this.btn_parse);
            this.Controls.Add(this.btn_dwnload);
            this.Controls.Add(this.listBox1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button btn_dwnload;
        private System.Windows.Forms.Button btn_parse;
        private System.Windows.Forms.Button btn_parse_bad;
    }
}

